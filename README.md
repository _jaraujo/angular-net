# Agenda de contatos #

### Objetivo ###

Criar uma agenda de contatos utilizando as tecnologias listadas abaixo 

### Tecnologias utilizadas: ###

* AngularJS
* .NET
* MongoDB

### Arquitetura ###

![material-starter-ux2](https://code.msdn.microsoft.com/site/view/file/136524/1/NewCurdApi.png)

### Como começar ###

* [Crie uma conta no Bitbucket](https://bitbucket.org/account/signin/?next=/)
* [Faça um fork do projeto](https://bitbucket.org/_jaraujo/angular-net/fork)