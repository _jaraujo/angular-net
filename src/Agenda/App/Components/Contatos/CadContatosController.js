﻿(function () {
    'use strict';

    angular
        .module('app')
        .controller('CadContatosController', CadContatoController);

    CadContatoController.$inject = ['$location']; 

    function CadContatoController($location) {
        /* jshint validthis:true */
        var vm = this;
        vm.title = 'CadContatoController';

        activate();

        function activate() { }
    }
})();
