﻿(function () {
    'use strict';

    angular
        .module('app')
        .controller('SearchContatosController', SearchContatoController);

    SearchContatoController.$inject = ['$location']; 

    function SearchContatoController($location) {
        /* jshint validthis:true */
        var vm = this;
        vm.title = 'SearchContatoController';

        activate();

        function activate() { }
    }
})();
