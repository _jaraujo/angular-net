﻿(function () {
    'use strict';

    angular
        .module('App', ['ngRoute'])
        .config(config);

    function config($routeProvider) {
        var basePath = '/Components/Contatos/';

        $routeProvider
            .when('/contatos', {
                templateUrl: basePath + 'SearchContatos.html',
                controller: 'SearchContatosController',
                controllerAs:'Contatos'
            })

            .when('/contatos/add', {
                templateUrl: basePath + 'CadContatos.html',
                controller: 'CadContatosController',
                controllerAs:'Contato'
            });
    }

})();